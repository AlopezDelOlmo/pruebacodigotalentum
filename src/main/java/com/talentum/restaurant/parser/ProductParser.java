package com.talentum.restaurant.parser;

import com.talentum.restaurant.Products;
import com.talentum.restaurant.stock.StockProducts;

public class ProductParser implements Parser<Products, StockProducts> {

    @Override
    public Products parse(StockProducts source) {

        Products product = new Products();

        if(source != null){
            product.setName(source.getName());
            product.setPrize(source.getPrize());
            product.setQuantity(source.getQuantity());

        }

        return product;
    }

    @Override
    public StockProducts reverseParse(Products source) {

        StockProducts stockProducts = new StockProducts();

        if(source != null) {
            stockProducts.setName(source.getName());
            stockProducts.setPrize(source.getPrize());
            stockProducts.setQuantity(source.getQuantity());
        }

        return stockProducts;
    }
}
