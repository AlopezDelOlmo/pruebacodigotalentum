package com.talentum.restaurant.parser;

public interface Parser<T, U> {

    T parse(U source);

    U reverseParse(T source);

}
