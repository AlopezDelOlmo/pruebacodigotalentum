package com.talentum.restaurant.stock;

import com.talentum.restaurant.Products;
import com.talentum.restaurant.parser.Parser;

import java.util.Map;

public class StockManager {

    private Map<String,StockProducts> restaurantStock;

    private Parser<Products,StockProducts> parser;

    public StockManager(Map<String, StockProducts> restaurantStock, Parser<Products, StockProducts> parser) {
        this.restaurantStock = restaurantStock;
        this.parser = parser;
    }

    public Products obtainProduct(String productName, Long quantityDesired) throws ProductNotFoundException, ProductEnoughException {

        StockProducts desiredProduct;

        if( (desiredProduct = restaurantStock.get(productName)) == null ) throw new ProductNotFoundException();
        if( !enoughStock(desiredProduct, quantityDesired)) throw new ProductEnoughException();

        restaurantStock.get(productName).setQuantity(restaurantStock.get(productName).getQuantity() - quantityDesired);

        return parser.parse(new StockProducts(desiredProduct.getName(), desiredProduct.getPrize(), quantityDesired));
    }

    private boolean enoughStock(StockProducts products, Long quantityDesired){
        return products.getQuantity() - quantityDesired > 0;
    }
}
