package com.talentum.restaurant.order;


import com.talentum.restaurant.Products;

import java.util.List;

public class Order {

    private List<Products> products;

    private OrderStatus status;

    public Order(List<Products> products, OrderStatus status) {
        this.products = products;
        this.status = status;
    }

    public List<Products> getProducts() {
        return products;
    }

    public void setProducts(List<Products> products) {
        this.products = products;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }
}
