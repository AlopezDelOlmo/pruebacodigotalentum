package com.talentum.restaurant.order;

public enum OrderStatus {
    PENDING, PURCHASED;
}
