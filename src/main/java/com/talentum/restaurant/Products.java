package com.talentum.restaurant;

import java.math.BigDecimal;

public class Products {

    private String name;

    private BigDecimal prize;

    private Long quantity;

    public Products() {
    }

    public Products(String name, BigDecimal prize) {
        this.name = name;
        this.prize = prize;
    }

    public Products(String name, BigDecimal prize, Long quantity) {
        this.name = name;
        this.prize = prize;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrize() {
        return prize;
    }

    public void setPrize(BigDecimal prize) {
        this.prize = prize;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
