package com.talentum.extractnumbers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NumberExtractor {

    private static final String SEPARATE_NUMBERS_REGEX = "\\d+";

    public static List<String> splitNumbers(String input) {

        List<String> numbers = new ArrayList<>();
        fillWithNumbers(input, numbers);
        return numbers;
    }


    private static void fillWithNumbers(String input, List<String> listOfNumbers){

        Pattern pattern = Pattern.compile(SEPARATE_NUMBERS_REGEX);
        Matcher matcher = pattern.matcher(input);
        while(matcher.find()) {
            listOfNumbers.add(matcher.group());
        }
    }

}
