package com.talentum.extractnumbers;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ExtractNumbers {

    public static List<Integer> getNumbers(String text) {

        List<String> strings = NumberExtractor.splitNumbers(text);

        Set<String> setWithoutDuplicates = strings.stream().collect(Collectors.toSet());

        return setWithoutDuplicates.stream().map(x -> Integer.valueOf(x)).sorted().collect(Collectors.toList());

    }
}
