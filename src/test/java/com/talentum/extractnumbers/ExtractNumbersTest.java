package com.talentum.extractnumbers;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ExtractNumbersTest {


    @Test
    public void testExtractNumbersCorrectInput(){

        String input = "A56B455VB23GTY23J";

        List<Integer> expectedResult = new ArrayList<>();
        expectedResult.add(23);
        expectedResult.add(56);
        expectedResult.add(455);

        List<Integer> result = ExtractNumbers.getNumbers(input);

        assertEquals(expectedResult, result);
    }

    @Test
    public void testExtractNumbersOnlyLetters(){

        String input = "ABCDEFG";

        List<Integer> expectedResult = new ArrayList<>();

        List<Integer> result = ExtractNumbers.getNumbers(input);

        assertEquals(expectedResult, result);
    }

    @Test
    public void testExtractNumbersOnlyNumbers(){

        String input = "123456";

        List<Integer> expectedResult = new ArrayList<>();
        expectedResult.add(123456);

        List<Integer> result = ExtractNumbers.getNumbers(input);

        assertEquals(expectedResult, result);
    }


}