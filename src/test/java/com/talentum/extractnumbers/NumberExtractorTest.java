package com.talentum.extractnumbers;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class NumberExtractorTest {

    @Test
    public void testExtractNumberStringCorrectString(){

        String input = "A56B455VB23GTY23J";

        List<String> expectedResult = new ArrayList<>();
        expectedResult.add("56");
        expectedResult.add("455");
        expectedResult.add("23");
        expectedResult.add("23");

        List<String> result = NumberExtractor.splitNumbers(input);

        assertEquals(expectedResult, result);
    }


    @Test
    public void testExtractNumberStringWithoutNumbers(){

        String input = "ABCDEFGHIJK";

        List<String> expectedResult = new ArrayList<>();

        List<String> result = NumberExtractor.splitNumbers(input);

        assertEquals(expectedResult, result);
    }

    @Test
    public void testExtractNumberStringOnlyNumbers(){

        String input = "123456789";

        List<String> expectedResult = new ArrayList<>();
        expectedResult.add("123456789");

        List<String> result = NumberExtractor.splitNumbers(input);

        assertEquals(expectedResult, result);
    }




}