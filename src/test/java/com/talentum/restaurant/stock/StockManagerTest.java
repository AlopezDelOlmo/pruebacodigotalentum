package com.talentum.restaurant.stock;

import com.talentum.restaurant.Products;
import com.talentum.restaurant.parser.ProductParser;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class StockManagerTest {

    private StockManager stockManager = new StockManager(getStock(), new ProductParser());

    @Test
    public void testGetProductFromStock() throws ProductEnoughException, ProductNotFoundException {

        String productName = "PROD1";
        Long quantityDesired = 1L;

        Products result = stockManager.obtainProduct(productName, quantityDesired);

        assertEquals(quantityDesired, result.getQuantity());

    }

    @Test(expected = ProductNotFoundException.class)
    public void testGetProductFromStockProductNotFound() throws ProductEnoughException, ProductNotFoundException {

        String productName = "PROD3";
        Long quantityDesired = 1L;

        Products result = stockManager.obtainProduct(productName, quantityDesired);

        assertEquals(quantityDesired, result.getQuantity());

    }

    @Test(expected = ProductEnoughException.class)
    public void testGetProductFromStockProductNotEnoughStock() throws ProductEnoughException, ProductNotFoundException {

        String productName = "PROD2";
        Long quantityDesired = 1L;

        Products result = stockManager.obtainProduct(productName, quantityDesired);

        assertEquals(quantityDesired, result.getQuantity());

    }

    private Map<String,StockProducts> getStock(){
        Map<String,StockProducts> stock = new HashMap<>();

        stock.put("PROD1", new StockProducts("PROD1", new BigDecimal(10), 2L));
        stock.put("PROD2", new StockProducts("PROD2", new BigDecimal(10), 0L));

        return stock;

    }

}